import { Authorized, Body, JsonController, Post, QueryParam, QueryParams, Param, Get, BadRequestError, Controller } from 'routing-controllers';

//import { queues, NOTIFY_URL } from '../queues/Queue';
import { Search } from '../entities/Search';
import { PuppeteerService } from '../services/PuppeteerService';
import { Product } from '../entities/Product';


@Controller('/search')
export class SearchController {

    constructor(private puppeteerService: PuppeteerService) {}

    @Post("/")
    public async Post(@Body() search: Search): Promise<Product[]> {
        if(search == null)        
            throw new BadRequestError("id Order is Null");
        
        return await this.puppeteerService.Search(search);
        
        //Producers Task 
        //queues[NOTIFY_URL].add("Search", {
        //    id: order.id
        //    });

       // return order;
    }    

    @Post("/full")
    public async Full(@Body() search: Search): Promise<Product[]> {
        if(search == null)        
            throw new BadRequestError("id Order is Null");
        
        return await this.puppeteerService.SearchOneToOne(search);
        
        //Producers Task 
        //queues[NOTIFY_URL].add("Search", {
        //    id: order.id
        //    });

       // return order;
    } 
}