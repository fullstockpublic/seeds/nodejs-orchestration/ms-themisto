import "reflect-metadata";
import * as dotenv from "dotenv";
import Container from 'typedi';

if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();
}

import { queues, Themisto, Ganymede } from './amqp/Queue';
import { PuppeteerService } from './services/PuppeteerService';
import { Product } from './entities/Product';

try {
    console.log("Run");
    queues[Themisto].process("Search", async (job, done) => {      
        
        console.log(`Job completed with result ${job.data}`);
        //Producers Task 
        try {
            let puppeteerService = Container.get(PuppeteerService);

            if (job.data.search != null) {
                console.log(`Job completed with result ${JSON.stringify(job.data.search)}`);

                const productList: Product[] = await puppeteerService.SearchOneToOne(job.data.search);        
                
                queues[Ganymede].add("OrderNotification", {
                    id: job.data.id,
                    productList: productList
                });

                done();
            } else {
                const errorLogic = `your work does not contain the search. `;
                done(new Error(errorLogic));
                console.log(errorLogic + ` ${JSON.stringify(job.data)}`);
            }
           
            console.log(`Job completed with result ${job}`);
        } catch (error) {
            done(error);
            console.log(`Job Error with ${error}`);
        }
    });
    queues[Themisto].on('error', function(error) {
        console.log(`Job completed with result ${error}`);
    });    
} catch (error) {
    console.log(`Catch |`);
    console.log(error);
}

