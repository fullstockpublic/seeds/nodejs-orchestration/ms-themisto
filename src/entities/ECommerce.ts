export class ECommerce {

    activated: boolean;

    domain: string;

    provider: string;

    optionalUser: boolean;

    loginSelector: LoginSelector;

    finderSelector: string;    

    listingSelector: ListingSelector;

    productSelector: ProductSelector;

    waitListing: string;

    viewport: Viewport;
}

export class Viewport {
    width: number;
    height: number;
}

export class ListingSelector {
    timeWait: number;
    productsSelector: string;
    priceSelector: string;
    nameSelector: string;
    discountSelector: string;
    imgUrlSelector: string;
    uriSelector: string;
}

export class ProductSelector {
    priceSelector: string;
    nameSelector: string;
    discountSelector: string;
    imgUrlsSelector: string;
    CodeSelector: string;
    IdentificationSelector: string;
}

export class LoginSelector {
    popUp: boolean;
    timeWait: number;
    url:string;
    actionsClick:string[];
    userSelector: string;
    passSelector: string;
    enterClick: string;
    waitLogin: string;
}

export class QuerySelector {
    urlQuery: string;
    PasswordSelector: string;
}