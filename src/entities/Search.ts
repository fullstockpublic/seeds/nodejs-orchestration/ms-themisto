export class Search {

    id: string;

    idOrder: string;

    searchQuery: string;

    provider: string;

    userOption: UserOption;
}

export class UserOption {

    user: string;

    pass: string;

}